#!/bin/bash
destination_branch="develop"
source_branch=$(date +%s | sha256sum | head -c 32 ; echo)
echo "Create local branch with tag " $source_branch
git config --global user.name "andycungkrinx"
git clone git@github.com-andycungkrinx91:andycungkrinx91/php-alpine.git $source_branch
cd $source_branch
git checkout -b $source_branch
git remote add upstream git@gitlab.com:andycungkrinx/php-alpine.git
git pull upstream master --allow-unrelated-histories
git checkout $destination_branch
git merge $source_branch --no-verify --allow-unrelated-histories
git add .
git commit -m "bump repo $source_branch"
git push origin $destination_branch